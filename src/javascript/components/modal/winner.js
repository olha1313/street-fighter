import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'span', className: 'modal-body' });
  bodyElement.innerText = `${fighter.name} just won this fight`;

  const onClose = () => location.reload();

  showModal({ title: 'We have a winner', bodyElement, onClose });
}
