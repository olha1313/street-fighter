import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const keys = new Map();
  const [firstFighterHealthBar, secondFighterHealthBar] = document.getElementsByClassName('arena___health-bar');
  const currentFirstFighter = { ...firstFighter, bar: firstFighterHealthBar, maxHealth: firstFighter.health, combo: 0 }
  const currentSecondFighter = { ...secondFighter, bar: secondFighterHealthBar, maxHealth: secondFighter.health, combo: 0 }

  return new Promise((resolve) => {
    function dealDamage (attacker, defender, damage) {
      if (defender.health > damage) {
        defender.health -= damage;
        defender.bar.style.width = `${defender.health / defender.maxHealth * 100}%`;
      } else {
        defender.health = 0;
        defender.bar.style.width = '0%';
        removeEventListener('keydown', keyDownListener);
        removeEventListener('keyup', keyUpListener);
        resolve(attacker);
      }
    }

    function keyDownListener ({ code }) {
      keys.set(code, true);

      if (code === controls.PlayerOneAttack && !keys.get(controls.PlayerOneBlock) && !keys.get(controls.PlayerTwoBlock)) {
        const damage = getDamage(firstFighter, secondFighter);
        dealDamage(currentFirstFighter, currentSecondFighter, damage);

      } else if (code === controls.PlayerTwoAttack && !keys.get(controls.PlayerTwoBlock) && !keys.get(controls.PlayerOneBlock)) {
        const damage = getDamage(secondFighter, firstFighter);
        dealDamage(currentSecondFighter, currentFirstFighter, damage);

      } else if (controls.PlayerOneCriticalHitCombination.includes(code)
        && controls.PlayerOneCriticalHitCombination.every((code) => keys.get(code))
        && !keys.get(controls.PlayerOneBlock)
        && Date.now() - currentFirstFighter.combo > 10000) {
        currentFirstFighter.combo = Date.now();
        const damage = firstFighter.attack * 2;
        dealDamage(currentFirstFighter, currentSecondFighter, damage);

      } else if (controls.PlayerTwoCriticalHitCombination.includes(code)
        && controls.PlayerTwoCriticalHitCombination.every((code) => keys.get(code))
        && !keys.get(controls.PlayerTwoBlock)
        && Date.now() - currentSecondFighter.combo > 10000) {
        currentSecondFighter.combo = Date.now();
        const damage = secondFighter.attack * 2;
        dealDamage(currentSecondFighter, currentFirstFighter, damage);
      }
    }

    function keyUpListener ({ code }) {
      keys.set(code, false)
    }

    addEventListener('keydown', keyDownListener);
    addEventListener('keyup', keyUpListener);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = random(1, 2);

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = random(1, 2);

  return defense * dodgeChance;
}

function random(min, max) {
  return min + Math.random() * (max - min);
}