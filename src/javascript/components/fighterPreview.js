import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'span', className: 'preview___fighter-name' });
    fighterName.innerText = fighter.name;
    const fightHealth = createElement({ tagName: 'span' })
    fightHealth.innerText = `Health - ${fighter.health}`;
    const fightAttack = createElement({ tagName: 'span' })
    fightAttack.innerText = `Attack - ${fighter.attack}`;
    const fightDefense = createElement({ tagName: 'span' })
    fightDefense.innerText = `Defense - ${fighter.defense}`;
    const fighterInfo = createElement({ tagName: 'div', className: 'preview___fighter-info' });
    fighterInfo.append(fightHealth, fightAttack, fightDefense);

    fighterElement.append(fighterImage, fighterName, fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
